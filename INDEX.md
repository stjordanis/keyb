# keyb

Keyboard driver (BIOS level) for international support


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## KEYB.LSM

<table>
<tr><td>title</td><td>keyb</td></tr>
<tr><td>version</td><td>2.01a</td></tr>
<tr><td>entered&nbsp;date</td><td>2013-12-14</td></tr>
<tr><td>description</td><td>Keyboard driver (BIOS level) for international support</td></tr>
<tr><td>keywords</td><td>keyb, keyboard, driver</td></tr>
<tr><td>author</td><td>Aitor SANTAMARIA MERINO</td></tr>
<tr><td>maintained&nbsp;by</td><td>freedos-devel@lists.sourceforge.net</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/keyb</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Keyb</td></tr>
</table>
